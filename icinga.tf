resource "icinga2_hostgroup" "my_first_hostgroup" {
  name         = "my_first_hostgroup"
  display_name = "My first hostgroup"
}

resource "icinga2_host" "google" {
  hostname      = "www.google.com"
  address       = "www.google.com"
  groups        = ["${icinga2_hostgroup.my_first_hostgroup.name}"]
  check_command = "hostalive"
  templates     = ["mail-service-notification"]

  vars = {
    os        = "linux"
    osver     = "1"
    allowance = "none"
  }
}

resource "icinga2_service" "http" {
  name          = "http"
  hostname      = "${icinga2_host.google.hostname}"
  check_command = "http"
}

resource "icinga2_user" "franta" {
  name  = "franta"
  email = "franta@ics.muni.cz"
}

#resource "icinga2_notification" "daportal-dev-notification" {
#  hostname = "${icinga2_host.google.hostname}"
#  command  = "mail-host-notification"
#  users    = ["franta"]
#}

#resource "icinga2_notification" "daportal-dev-http-notification" {
#  hostname    = "${icinga2_host.google.hostname}"
#  command     = "mail-service-notification"
#  users       = ["franta"]
#  servicename = "${icinga2_service.http.name}"
#}
