# Using Icinga API with Terraform - simple example

## Install Terraform

Install Terraform from
https://www.terraform.io/downloads.html
~~The older the 0.11.14 version has to be used, because Icinga is not yet supported in the current 12.* version.~~

## Clone this repository

```
git clone https://gitlab.ics.muni.cz/2780/tf-icinga-example
cd tf-icinga-example
```
## Insert your Icinga credentials
Edit *secrets.sh* file and insert you Icinga credetial in place of placeholders API_USER_PLACEHOLDER and API_PASSWORD_PLACEHOLDER.

## Source your Icinga credentials

```
source secrects.sh
```

## Initialize terraform

```
terraform init
```

## Show plan
```
terraform plan
```

## Apply plan

```
terraform apply
```

## Check results in icinga

## Remove config from Icinga

```
terraform destroy
```
Now you can check documentation at https://www.terraform.io/docs/providers/icinga2/index.html, edit configuration in icinga.tf nad repeate the cycle
 * terraform plan
 * terraform apply
 * (terraform destroy)
