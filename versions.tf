terraform {
  required_providers {
    icinga2 = {
      source = "icinga/icinga2"
    }
  }
}
